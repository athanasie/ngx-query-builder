import { Component, ViewEncapsulation, OnInit, AfterViewInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { BasicQueryElement, CompositeQueryElement } from './ngx-query-builder.service';

@Component({
    selector: 'universis-query-composite-element',
    templateUrl: './ngx-composite-query-element.component.html',
    styleUrls: [
        './ngx-query-element.component.scss'
    ],
    encapsulation: ViewEncapsulation.None
  })
  export class QueryCompositeElementComponent implements OnInit, AfterViewInit, OnChanges {

    @Input() element: CompositeQueryElement;
    @Output() remove = new EventEmitter();
    @Input() removable: boolean;
    @Input() elementTemplate: any = null;
    @Input() customParams: any = {};

    ngOnInit(): void {
        //
    }
    ngAfterViewInit(): void {
        //
    }
    ngOnChanges(changes: SimpleChanges): void {
        //
    }

    addFilter() {
      this.element.elements.push({
        type: 'BinaryExpression',
        operator: '$eq',
        left: null,
        right: null
      } as BasicQueryElement)
    }

    addInnerGroup(operator?: '$or' | '$and') {
      this.element.elements.push({
        operator: operator || '$and',
        type: 'LogicalExpression',
        elements: [
          {
            type: 'BinaryExpression',
            operator: '$eq',
            left: null,
            right: null
          },
          {
            type: 'BinaryExpression',
            operator: '$eq',
            left: null,
            right: null
          }
        ]
      } as CompositeQueryElement)
    }

    onRemove(index: number) {
      this.element.elements.splice(index, 1);
      if (this.element.elements.length === 0) {
        this.remove.emit();
      }
    }

    switchGroup(operator: '$and' | '$or') {
      this.element.operator = operator;
    }

  }