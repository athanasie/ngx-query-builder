import { Component, ViewEncapsulation, OnInit, AfterViewInit, OnChanges, SimpleChanges, Input, ViewChild, Output, OnDestroy } from '@angular/core';
import { BasicQueryElement, FormComponent, QueryBuilderService } from './ngx-query-builder.service';
import { TextUtils } from '@themost/client';
import { EventEmitter } from '@angular/core';
import { FormioComponent, FormioRefreshValue } from 'angular-formio';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { FormatWidth, getLocaleDateFormat } from '@angular/common';
@Component({
    selector: 'universis-query-element',
    templateUrl: './ngx-query-element.component.html',
    styleUrls: [
        './ngx-query-element.component.scss'
    ],
    encapsulation: ViewEncapsulation.None
  })
  export class QueryElementComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
    private changeSubscription: Subscription;
    private customEventSubscription: Subscription;

    constructor(protected translateService: TranslateService, protected queryBuilderService: QueryBuilderService) {
      //
    }
  ngOnDestroy(): void {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    if (this.customEventSubscription) {
      this.customEventSubscription.unsubscribe();
    }
  }

    @Input() element: BasicQueryElement = {
      left: null,
      operator: '$eq',
      right: null
    };
    @Input() template: any;
    @Input() customParams: any = {};
    @Input() removable: boolean;
    public loading = true;
    @Output() remove = new EventEmitter();
    @ViewChild('form') form: FormioComponent;
    public refresh = new EventEmitter<FormioRefreshValue>()

    public property: { label: string; value: string; type: string };

    ngOnInit(): void {
        
    }
    ngAfterViewInit(): void {
      const left = this.template.properties.find((item) => this.element.left != null && this.element.left.value === item.value);
      const operator = this.element.operator || '$eq';
      const right = this.element.right;
      const removable = this.removable;
      this.form.submission = {
        data: {
          left,
          operator,
          right,
          removable
        }
       };
       const currentLang = this.translateService.currentLang;
       const customParams = this.customParams || {};
       Object.assign(this.template, {
          customParams
       });

       // set date time format
       const shortDateFormat = getLocaleDateFormat(this.translateService.currentLang, FormatWidth.Short);
       const dateComponent = this.queryBuilderService.findComponent(this.template as FormComponent, 'Edm.DateTimeOffset');
       if (dateComponent) {
        dateComponent.format = shortDateFormat;
        if (dateComponent.widget) {
          dateComponent.widget.format = shortDateFormat;
        }
       }
       
       this.form.setForm(this.template);
       const resources = this.form.formio.i18next.options.resources;
       if (Object.prototype.hasOwnProperty.call(resources, currentLang) === false) {
        resources[currentLang] = {
          translation: {}
        };
       }
       if (Object.prototype.hasOwnProperty.call(resources, 'QueryBuilder') === false) {
          const translation = this.translateService.instant('QueryBuilder');
          Object.assign(resources[currentLang].translation, translation);
          Object.defineProperty(resources, 'QueryBuilder', {
            configurable: true,
            enumerable: false,
            writable: true,
            value: true
          });
       }
       
       this.form.formio.language = currentLang;
       
       this.refresh.emit({
        submission: this.form.submission
      });

       this.form.formLoad.subscribe(() => {
        setTimeout(() => {
          this.loading = false;
        }, 50);
       });
       this.changeSubscription = this.form.change.subscribe((event: any) => {
        if (event && event.data) {
          if (event.changed && event.changed.component && event.changed.component.key === 'left') {
            const submission = this.form.formio.submission;
            submission.data.right = event.data.right = "";
            setTimeout(() => {
              return this.refresh.emit({
                submission
              });
            }, 0);
          }
          this.element.left = event.data.left;
          this.element.operator = event.data.operator;
          this.element.right = event.data.right;
          if (TextUtils.isDate(event.data.right)) {
            this.element.right = event.data.right.substring(0,10);
          }
        }
      });
      this.customEventSubscription = this.form.customEvent.subscribe((event: any) => {
        if (event.type === 'remove') {
          this.remove.emit();
        }
      });
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.removable != null && changes.removable.firstChange === false) {
          const submission = this.form.formio.submission;
          submission.data.removable = changes.removable.currentValue;
          this.refresh.emit({
            submission
          });
        }
    }

  }